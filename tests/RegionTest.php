<?php
/*
 * @Author: Danny.Lu <dannytiehui@hotmail.com>
 * @Date: 2020-09-21 18:14:57
 * @LastEditTime: 2020-10-16 18:57:01
 * @Copyright: 2020 Lu Tie Hui (2^1024)
 */

namespace Kabel\ChinaRegion\Tests;

use Kabel\ChinaRegion\Models\Region;
use Kabel\ChinaRegion\Repositories\RegionRepository;
use Kabel\ChinaRegion\Services\RegionService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class RegionTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * @group ChinaRegion-Select
     *
     * @return void
     */
    public function testSelectMask()
    {
        $regionRepository = new RegionRepository(new Region());

        // 查找1、2、3、7
        $mask = 1;
        $d = $regionRepository->selectMask($mask);

        Log::info("testSelectMask");
        Log::info($d->toArray());
        Log::info($d->toJSON());
        
        $this->assertTrue(count($d) ? true:false);
    }

    /**
     * @group ChinaRegion-Select
     *
     * @return void
     */
    public function testSelectSub()
    {
        $regionRepository = new RegionRepository(new Region());

        // 查找下一级
        $regionId = 430781000000; // 津市
        $d = $regionRepository->selectSub($regionId);

        Log::info("testSelectSub");
        Log::info($d->toArray());
        Log::info($d->toJSON());
        
        $this->assertTrue(count($d) ? true:false);
    }

    /**
     * @group ChinaRegion-Select
     *
     * @return void
     */
    public function testSelectSub2()
    {
        $regionRepository = new RegionRepository(new Region());
        
        // 查找下二级
        $regionId = 430700000000; // 常德
        $d = $regionRepository->selectSub2($regionId);

        Log::info("testSelectSub2");
        Log::info($d->toArray());
        Log::info($d->toJSON());
        
        $this->assertTrue(count($d) ? true:false);
    }

    /**
     * @group ChinaRegion-Select
     *
     * @return void
     */
    public function testSelectSup()
    {
        $regionRepository = new RegionRepository(new Region());

        // 反向查找
        $regionId = 430781000000; // 津市
        $d = $regionRepository->selectSup($regionId);

        Log::info("testSelectSup");
        Log::info($d->toArray());
        Log::info($d->toJSON());
        
        $this->assertTrue(count($d) ? true:false);
    }

    // public function testSelectByIdAndLevel()
    // {
    //     $regionService = new RegionService(new RegionRepository(new Region()));

    //     Log::info("testSelectByIdAndLevel");

    //     // 查找【省】
    //     $d = $regionService->selectByIdAndLevel(0, 1);
    //     Log::info("id=0, level=1");
    //     Log::info($d->toArray());
    //     Log::info($d->toJSON());

    //     // 查找【市】
    //     $d = $regionService->selectByIdAndLevel(0, 2);
    //     Log::info("id=0, level=2");
    //     Log::info($d->toArray());
    //     Log::info($d->toJSON());

    //     // 查找【省+市】
    //     $d = $regionService->selectByIdAndLevel(0, 3);
    //     Log::info("id=0, level=3");
    //     Log::info($d->toArray());
    //     Log::info($d->toJSON());

    //     // 查找【省+市+区】
    //     $d = $regionService->selectByIdAndLevel(0, 7);
    //     Log::info("id=0, level=7");
    //     Log::info($d->toArray());
    //     Log::info($d->toJSON());

    //     // 查找上N级
    //     $regionId = 430781000000; // 津市
    //     $d = $regionService->selectByIdAndLevel($regionId, 0);
    //     Log::info("id=$regionId, level=0");
    //     Log::info($d->toArray());
    //     Log::info($d->toJSON());

    //     // 查找下一级
    //     $regionId = 430781000000; // 津市
    //     $d = $regionService->selectByIdAndLevel($regionId, 1);
    //     Log::info("id=$regionId, level=1");
    //     Log::info($d->toArray());
    //     Log::info($d->toJSON());

    //     // 查找下二级
    //     $regionId = 430700000000; // 常德
    //     $d = $regionService->selectByIdAndLevel($regionId, 2);
    //     Log::info("id=$regionId, level=2");
    //     Log::info($d->toArray());
    //     Log::info($d->toJSON());
        
    //     $this->assertTrue(count($d) ? true:false);
    // }
}
