<?php
/*
 * @Author: Danny.Lu <dannytiehui@hotmail.com>
 * @Date: 2020-07-11 15:03:49
 * @LastEditTime: 2020-10-10 17:43:39
 * @Copyright: 2020 Lu Tie Hui (2^1024)
 */

namespace Kabel\ChinaRegion\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Systems\Region\Models\Region
 *
 * @property int $id 12位区划代码
 * @property string $name 名称
 * @property int|null $code 城乡分类代码
 * @property string|null $py 名称拼音首字母
 * @property string|null $pinyin 名称拼音全拼
 * @property int $level 级别：1省份；2城市；4区域；8镇/街道；16居委会
 * @property int $status 状态：1启用/已开通；2禁用/未开通
 * @property int $parent_id 父id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Systems\Region\Models\Region newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Systems\Region\Models\Region newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Systems\Region\Models\Region query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Systems\Region\Models\Region whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Systems\Region\Models\Region whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Systems\Region\Models\Region whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Systems\Region\Models\Region whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Systems\Region\Models\Region whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Systems\Region\Models\Region whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Systems\Region\Models\Region whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Systems\Region\Models\Region wherePinyin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Systems\Region\Models\Region wherePy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Systems\Region\Models\Region whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Systems\Region\Models\Region whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Region extends Model
{
    protected $table = 'china_regions';
    // protected $visible = ['id', 'name', 'level', 'parent_id'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * 模型的默认属性值。
     *
     * @var array
     */
    protected $attributes = [
        'status' => 1,
    ];
}
