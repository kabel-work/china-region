<?php

namespace Kabel\ChinaRegion\Facades;

use Illuminate\Support\Facades\Facade;

class ChinaRegion extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'china-region';
    }
}
