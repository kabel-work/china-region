<?php
 
namespace Kabel\ChinaRegion\Repositories;

use Kabel\ChinaRegion\Interfaces\RegionInterface;
use Kabel\ChinaRegion\Models\Region;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Collection;

class RegionRepository implements RegionInterface
{
    /**
     * Eloquent 模型对象
     *
     * @var Kabel\ChinaRegion\Models\Region
     */
    protected $model;

    public function __construct(Region $model)
    {
        $this->model = $model;
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    /**
     * 查找掩码值对应的数据
     *
     * @param integer $mask 1=省；2=市；4=区；1+2=3（省+市）；2+4=6（市+区）；1+2+4=7（省+市+区）
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function selectMask($mask = 1)
    {
        $province = 1 << 0;
        $city = 1 << 1;
        $county = 1 << 2;
        // $town = 1 << 3;
        // $village = 1 << 4;

        $collection = new Collection();

        if ($mask & $province) {
            $provinceCollection = Region::select()->where('level', $province)->get();
            $collection = $collection->merge($provinceCollection);
        }

        if ($mask & $city) {
            $cityCollection = Region::select()->where('level', $city)->get();
            $collection = $collection->merge($cityCollection);
        }

        if ($mask & $county) {
            $countyCollection = Region::select()->where('level', $county)->get();
            $collection = $collection->merge($countyCollection);
        }

        // if ($mask & $town) {
        //     $townCollection = Region::select()->where('level', $town)->get();
        //     $collection = $collection->merge($townCollection);
        // }

        // if ($mask & $village) {
        //     $villageCollection = Region::select()->where('level', $village)->get();
        //     $collection = $collection->merge($villageCollection);
        // }
        
        return $collection;
    }
    
    /**
     * 查找下一级数据
     *
     * @param integer $id
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function selectSub($id = 0)
    {
        return Region::select()->where('parent_id', $id)->get();
    }

    /**
     * 查找下一级 + 下二级数据
     *
     * @param integer $id
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function selectSub2($id = 0)
    {
        $regionCollection = $this->selectSub($id);
        $ids = array();
        foreach ($regionCollection as $region) {
            $ids[] = $region->id;
        }

        $regionCollection2 = Region::select()->whereIn('parent_id', $ids)->get();

        return $regionCollection->merge($regionCollection2);
    }

    /**
     * 查找上N级，直到找到省级
     *
     * @param integer $id
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function selectSup($id = 0)
    {
        $collection = new Collection();

        $region = Region::find($id);
        $collection = $collection->push($region);
        $parent_id = $region->parent_id;
        while ($parent_id != 1) {
            $temp = Region::find($parent_id);
            $collection = $collection->push($temp);
            $parent_id = $temp->parent_id;
        }

        return $collection;
    }

    

    /**
     * 查找指定级别数据
     *
     * @param integer $level
     *                  0. 向上查找N级，包括自己及所有父节点（递归向上查找到省）- 特殊情况
     *                  1. 省   (31条数据）- 查询所有省份列表 - 默认
     *                  2. 市   (342条数据)
     *                  4. 区   (条数据)
     *                  8. 镇/街道  (条数据)
     *                  16. 居委会  (条数据, 暂不支持)
     *                  3. 省+市    (条数据)
     *                  7. 省+市+区 (条数据)
     *                  15. 省+市+区+镇/街道    (条数据，暂不支持)
     *                  31. 省+市+区+镇/街道+居委会 （暂不支持）
     * @param integer $id
     *                  a=0时，传任意id
     *                  a=1时，传入国家id，中国为0，
     *                  a=2是，传入 省 id
     *                  a=4时，传入 市 id
     *                  a=8时，传入 区 id
     *                  a=16时，传入 街道/镇 id
     *
     * @return void
     */
}
