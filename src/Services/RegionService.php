<?php
/*
 * @Author: Danny.Lu <dannytiehui@hotmail.com>
 * @Date: 2020-07-21 01:57:18
 * @LastEditTime: 2020-10-10 17:59:11
 * @Copyright: 2020 Lu Tie Hui (2^1024)
 */

namespace Kabel\ChinaRegion\Services;

use Illuminate\Support\Collection;
use Kabel\ChinaRegion\Repositories\RegionRepository;

class RegionService
{
    /**
     * Eloquent 模型辅助对象 Repository
     *
     * @var App\ThirdParty\ChinaRegion\Repositories\RegionRepository
     */
    protected $repository;

    public function __construct(RegionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * 查找 省、市、区、镇/街道 四级行政区域对应数据接口
     *
     * @param integer $id
     * @param integer $level
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function selectByIdAndLevel($id = 0, $level = 0)
    {
        $collection = new Collection();

        if ($id == 0) {
            switch ($level) {
                case 1: // 返回【省】
                case 2: // 返回【市】
                case 3: // 返回【省+市】
                case 7: // 返回【省+市+区】
                    $collection = $this->repository->selectMask($level);
                    break;
                default:
            }
        } else {
            switch ($level) {
                case 0: // 返回上N级，包括自己及所有父节点（递归向上查找到省）
                    $collection = $this->repository->selectSup($id);
                    break;
                case 1: // 返回下一级
                    $collection = $this->repository->selectSub($id);
                    break;
                case 2: // 返回下二级
                    $collection = $this->repository->selectSub2($id);
                    break;
                default:
            }
        }

        return $collection;
    }
}
