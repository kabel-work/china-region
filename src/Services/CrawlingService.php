<?php

namespace Kabel\ChinaRegion\Services;

use Kabel\ChinaRegion\Interfaces\CrawlingInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use PhpParser\Node\Stmt\TryCatch;
use QL\QueryList;

// use QL\Ext\CurlMulti;
 
class CrawlingService implements CrawlingInterface
{
    protected $baseUrl;
    protected $provinceConfig;
    protected $cityConfig;
    protected $countyConfig;
    protected $townConfig;
    protected $villageConfig;
    protected $inCharset = 'GBK';
    protected $outCharset = 'UTF-8';

    public function __construct()
    {
        $this->inCharset = config('crawlingregion.in_charset');
        $this->outCharset = config('crawlingregion.out_charset');
        $this->baseUrl = config('crawlingregion.base_url');
        $this->provinceConfig = config('crawlingregion.province');
        $this->cityConfig = config('crawlingregion.city');
        $this->countyConfig = config('crawlingregion.county');
        $this->townConfig = config('crawlingregion.town');
        $this->villageConfig = config('crawlingregion.village');
    }

    public function curl_get_contents($url, $inCharset = 'GBK', $outCharset = 'UTF-8')
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $html = curl_exec($ch);
        curl_close($ch);

        $html = iconv($inCharset, $outCharset, $html);
        return ($html);
    }

    private function crawling($arr, $config)
    {
        $data = array();
        
        $count = count($arr);
        for ($i=0; $i < $count; $i++) {

            // href = '' 的情况，例如：广州市 市辖区 440101000
            if (!strlen($arr[$i]["href"])) {
                continue;
            }

            try {
                // 方法一（弃用）
                // $html = iconv($this->inCharset, $this->outCharset, file_get_contents($dataArr[$i]['url']));
                // 方法二（推荐）
                $html = $this->curl_get_contents($arr[$i]['url']);

                $temp = QueryList::html($html)
                ->rules($config['rules'])
                ->range($config['range'])
                ->queryData();
                $data = array_merge($data, $temp);
            } catch (\Exception $e) {
                Log::info($arr[$i]['url']);
            }
        }

        return $data;
    }

    public function crawlingProvince($href = 'index.html')
    {
        $html = $this->curl_get_contents($this->baseUrl . $href);
        $dataProvince = QueryList::html($html)
                        ->rules($this->provinceConfig['rules'])
                        ->range($this->provinceConfig['range'])
                        ->queryData();

        $count = count($dataProvince);
        for ($i=0; $i < $count; $i++) {
            if (!strlen($dataProvince[$i]["href"])) {
                unset($dataProvince[$i]);
                continue;
            }
            
            $index = strpos($dataProvince[$i]["href"], ".");
            $dataProvince[$i]["id"] = substr($dataProvince[$i]["href"], 0, $index);
        }
        
        return $dataProvince;
    }

    public function crawlingCity(array $province)
    {
        $count = count($province);
        for ($i=0; $i < $count; $i++) {
            $province[$i]['url'] = $this->baseUrl . $province[$i]['href'];
        }

        return $this->crawling($province, $this->cityConfig);
    }

    public function crawlingCounty(array $city)
    {
        $ret = array();

        $dataCounty = array();
        $noSubData = array();

        $count = count($city);
        for ($i=0; $i < $count; $i++) {
            $city[$i]['url'] = $this->baseUrl . $city[$i]['href'];
            
            try {
                $html = $this->curl_get_contents($city[$i]['url']);

                $temp = QueryList::html($html)
                        ->rules($this->countyConfig['rules'])
                        ->range($this->countyConfig['range'])
                        ->queryData();

                if (count($temp) == 0) {
                    $noSubData[] = $city[$i];
                } else {
                    $dataCounty = array_merge($dataCounty, $temp);
                }
            } catch (\Exception $e) {
                Log::info($city[$i]['url']);
            }
        }

        $ret['data'] = $dataCounty;
        $ret['noSubData'] = $noSubData;

        return $ret;
    }

    public function crawlingTown(array $county, array $noCountyCityData = [])
    {
        $count = count($county);
        for ($i=0; $i < $count; $i++) {
            if (!strlen($county[$i]["href"])) {
                continue;
            }

            $index = strpos($county[$i]["href"], "/");
            $provinceId = substr($county[$i]["href"], $index+1, 2);
            $county[$i]['url'] = $this->baseUrl . $provinceId . "/" . $county[$i]['href'];
        }

        // 补充 市下级没有 3.区，直接是 4. 镇/街道 的情况。例如：广东省东莞市 广东省中山市 海南省儋州市
        $count = count($noCountyCityData);
        for ($i=0; $i < $count; $i++) {
            $noCountyCityData[$i]['url'] = $this->baseUrl . $noCountyCityData[$i]['href'];
        }

        $ret['data'] = $this->crawling($county, $this->townConfig);
        $ret['noSubData'] = $this->crawling($noCountyCityData, $this->townConfig);

        return $ret;
    }

    public function crawlingVillage(array $town, array  $noCountyTownData = [])
    {
        $count = count($town);
        for ($i=0; $i < $count; $i++) {
            if (!strlen($town[$i]["href"])) {
                continue;
            }

            $index = strpos($town[$i]["href"], "/");
            $provinceId = substr($town[$i]["href"], $index+1, 2);
            $cityId = substr($town[$i]["href"], $index+3, 2);
            $town[$i]['url'] = $this->baseUrl . $provinceId . "/" . $cityId . "/" . $town[$i]['href'];
        }

        // 补充 市下级没有 3.区/镇，直接是 4.街道 的情况。例如：东莞市 - (没有区) - 东城街道441900003000
        $count = count($noCountyTownData);
        for ($i=0; $i < $count; $i++) {
            $index = strpos($noCountyTownData[$i]["href"], "/");
            $provinceId = substr($noCountyTownData[$i]["href"], $index+1, 2);
            $noCountyTownData[$i]['url'] = $this->baseUrl . $provinceId . "/" . $noCountyTownData[$i]['href'];
        }

        $ret['data'] = $this->crawling($town, $this->villageConfig);
        $ret['noSubData'] = $this->crawling($noCountyTownData, $this->villageConfig);

        return $ret;
    }

    public function crawlingMask($mask = 1)
    {
        $province = 1 << 0;
        $city = 1 << 1;
        $county = 1 << 2;
        $town = 1 << 3;
        $village = 1 << 4;

        $data = array();
        
        if ($mask & $province) {
            print_r('province-');
            $dataProvince = $this->crawlingProvince();
            $data['province'] = $dataProvince;

            $sql = "INSERT INTO `china_regions` (`id`, `name`, `level`, `parent_id`) VALUES";
            foreach ($data['province'] as $d) {
                $sql .= "\r\n(" . $d['id'] . ", '" . $d['name'] ."', $province, 1),";
            }
            Storage::disk('public')->put('china_regions_1.sql', $sql);
        }

        if ($mask & $city) {
            print_r('city-');
            $dataCity = $this->crawlingCity($dataProvince);
            $data['city'] = $dataCity;

            $sql = "INSERT INTO `china_regions` (`id`, `name`, `level`, `parent_id`) VALUES";
            foreach ($data['city'] as $d) {
                $parent_id = substr($d['id'], 0, 2);
                $sql .= "\r\n(" . $d['id'] . ", '" . $d['name'] ."', $city, $parent_id),";
            }
            Storage::disk('public')->put('china_regions_2.sql', $sql);
        }

        if ($mask & $county) {
            print_r('county-');
            $dataCounty = $this->crawlingCounty($dataCity);
            $data['county'] = $dataCounty['data'];

            $sql = "INSERT INTO `china_regions` (`id`, `name`, `level`, `parent_id`) VALUES";
            foreach ($data['county'] as $d) {
                $parent_id = substr($d['id'], 0, 4) . "00000000";
                $sql .= "\r\n(" . $d['id'] . ", '" . $d['name'] ."', $county, $parent_id),";
            }
            Storage::disk('public')->put('china_regions_4.sql', $sql);
        }

        if ($mask & $town) {
            print_r('town-');
            $dataTown = $this->crawlingTown($dataCounty['data'], $dataCounty['noSubData']);
            $data['town'] = array_merge($dataTown['data'], $dataTown['noSubData']);

            $sql = "INSERT INTO `china_regions` (`id`, `name`, `level`, `parent_id`) VALUES";
            foreach ($data['town'] as $d) {
                $parent_id = substr($d['id'], 0, 6) . "000000";
                $sql .= "\r\n(" . $d['id'] . ", '" . $d['name'] ."', $town, $parent_id),";
            }
            Storage::disk('public')->put('china_regions_8.sql', $sql);
        }

        // if ($mask & $village) {
        //     print_r('village-');
        //     $dataVillage = $this->crawlingVillage($dataTown['data'], $dataTown['noSubData']);
        //     $data['village'] = array_merge($dataVillage['data'], $dataVillage['noSubData']);

        //     $sql = "INSERT INTO `china_regions` (`id`, `name`,  `code`, `level`, `parent_id`) VALUES";
        //     foreach ($data['village'] as $d) {
        //         $parent_id = substr($d['id'], 0, 6) . "000000";
        //         $sql .= "\r\n(" . $d['id'] . ", '" . $d['name'] . "', " . $d['code'] . ", $village, $parent_id),";
        //     }
        //     Storage::disk('public')->put('china_regions_16.sql', $sql);
        // }

        return $data;
    }

    public function crawlingMaskTest($mask = 1, $dataCity = [])
    {
        $province = 1 << 0;
        $city = 1 << 1;
        $county = 1 << 2;
        $town = 1 << 3;
        $village = 1 << 4;

        Log::info("\r\n mask = $mask \r\n ");

        $data = array();
        
        // if ($mask & $province) {
        //     print_r('province-');
        //     $dataProvince = $this->crawlingProvince();
        //     $data['province'] = $dataProvince;
        // }

        // if ($mask & $city) {
        //     print_r('city-');
        //     $dataCity = $this->crawlingCity($dataProvince);
        //     $data['city'] = $dataCity;
        // }

        if ($mask & $county) {
            print_r('county-');
            $dataCounty = $this->crawlingCounty($dataCity);
            $data['county'] = $dataCounty['data'];
        }

        if ($mask & $town) {
            print_r('town-');
            $dataTown = $this->crawlingTown($dataCounty['data'], $dataCounty['noSubData']);
            $data['town'] = array_merge($dataTown['data'], $dataTown['noSubData']);
        }

        // if ($mask & $village) {
        //     print_r('village-');
        //     $dataVillage = $this->crawlingVillage($dataTown['data'], $dataTown['noSubData']);
        //     $data['village'] = array_merge($dataVillage['data'], $dataVillage['noSubData']);
        // }

        return $data;
    }
}
