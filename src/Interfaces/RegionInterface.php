<?php
/*
 * @Author: Danny.Lu <dannytiehui@hotmail.com>
 * @Date: 2020-07-21 17:20:12
 * @LastEditTime: 2020-10-16 18:02:39
 * @Copyright: 2020 Lu Tie Hui (2^1024)
 */

namespace Kabel\ChinaRegion\Interfaces;

interface RegionInterface
{
    const VERSION = '1.2020.1015';

    /**
     * 查找 掩码值对应的数据
     *
     * @param integer $mask 1=省；2=市；4=区；1+2=3（省+市）；2+4=6（市+区）；1+2+4=7（省+市+区）
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function selectMask($mask = 1);

    /**
     * 查找 下一级数据
     *
     * @param integer $id
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function selectSub($id = 0);

    /**
     * 查找 下一级 + 下二级数据
     *
     * @param integer $id
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function selectSub2($id = 0);

    /**
     * 查找 上N级，直到找到省级
     *
     * @param integer $id
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function selectSup($id = 0);
}
