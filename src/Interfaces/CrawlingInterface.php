<?php
/*
 * @Author: Danny.Lu <dannytiehui@hotmail.com>
 * @Date: 2020-07-11 14:49:27
 * @LastEditTime: 2020-10-16 18:01:55
 * @Copyright: 2020 Lu Tie Hui (2^1024)
 */

namespace Kabel\ChinaRegion\Interfaces;

interface CrawlingInterface
{
    const VERSION = '1.2020.1015';

    /**
     * 抓取 1级.省份 数据
     *
     * @param string $href 相对链接
     * @return array
     */
    public function crawlingProvince($href = 'index.html');

    /**
     * 抓取 2级.城市 数据
     *
     * @param array $province 省份数组
     * @return array
     */
    public function crawlingCity(array $province);

    /**
     * 抓取 3级.区域 数据
     *
     * 注意：广东省东莞市、中山市，海南省儋州市，这三座城市没有区，直接是 镇/街道 级行政区域
     *
     * @param array $city 城市数组
     * @return array ['data' => [], 'noSubData' => []]
     */
    public function crawlingCounty(array $city);

    /**
     * 抓取 4级. 镇/街道 数据
     *
     * @param array $county    3级.区域 数组
     * @param array $noCountyCityData  “2级.城市 没有 3级.区域” 的“城市”数组
     * @return array ['data' => [], 'noSubData' => []]
     */
    public function crawlingTown(array $county, array $noCountyCityData = []);

    /**
     * 抓取 5级. 居委会 数据
     *
     * @param array $town   4级.镇/街道 数组
     * @param array $noCountyTownData  “2级.城市 没有下级 “3.区域” 的 的“镇/街道”数组
     * @return array ['data' => [], 'noSubData' => []]
     */
    public function crawlingVillage(array $town, array $noCountyTownData = []);

    /**
     * 抓取指定级别数据
     *  1. 省   (31条数据）
     *  2. 市   (342条数据)
     *  4. 区   (3240条数据)
     *  8. 镇/街道  (41781条数据)
     *  16. 居委会  [条数据, 暂不支持]
     *  3. 省+市    (373条数据)
     *  7. 省+市+区 (3613条数据)
     *  15. 省+市+区+镇/街道    (45394条数据)
     *  31. 省+市+区+镇/街道+居委会 [暂不支持]
     *
     * @param integer $mask
     * @return array ['province' => [], 'city' => [], 'county' => [], 'town' => [], 'village' => []]
     */
    public function crawlingMask($mask = 1);
}
